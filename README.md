Changelog

# V7

Init FreeCAD.

## V7.1 

La tolerancia en las columnas estaba mal, en vez de restar 0.2 mm estaba sumando 0.2 mm. Una "anti-tolerancia". Esto hacia que la tolerancia en los agujeros de la base fuera contrarrestada.

Ahora está como debería: la base y la tapa no tienen tolerancia y las columnas si.

Tambien borré el archivo de FreeCAD con la columna por separado, deje solo el "bot". Que tiene la columnita.

## v7.2

Bugfix: el "wedge" de la columnita estaba descentrado, por eso no encajaba.

Ademas la impresora tenia el first layer demasiado bajo (estaba en -5, y ahora esta en -4.3).

## v7.3 branding

Gradilla PA

Texto con:

* https://wiki.freecadweb.org/Draft_ShapeString_tutorial
* https://www.youtube.com/watch?v=SEf-FJ3HLQg
* `/tmp/DejaVuSans.ttf`
